import * as React from 'react';
import { StyleSheet, Modal, Alert, TextInput, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';


import { Text, View } from '../components/Themed';
import { SimpleLineIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome5 } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';
import { setUser, setRoom, setLogout, setRoomChat } from '../Reducer/UserReducer';
import TextAvatar from 'react-native-text-avatar'
import { rootRef } from '../firebase/firebase';

export default function TabProfileScreen() {

  const navigation = useNavigation();

  const userRedux = useSelector(redux => redux.User)

  // console.log("Status", userRedux.status)
  console.log("key", userRedux.genkey)
  const [modalVisibleCreateRoom, setModalVisibleCreateRoom] = React.useState(false);
  const [modalVisibleLogout, setModalVisibleLogout] = React.useState(false);

  const [roomName, setRoomName] = React.useState("");
  const [password, setPassword] = React.useState("");


  // console.log("userRedux", userRedux)
  const dispatch = useDispatch()

  // const OnCreateRoom =() =>{
  //   dispatch(setRoom(roomName,password))
  //   setModalVisibleCreateRoom(!modalVisibleCreateRoom)
  // }

  React.useEffect(() => {
    checkStatus()
  }, [])

  const OnCreateRoomNew = async () => {
    setPassword("")
    setRoomName("")
    dispatch(setRoomChat(userRedux.name, roomName, password))
    dispatch(setRoom(roomName, password))
    setModalVisibleCreateRoom(!modalVisibleCreateRoom)
    await rootRef.ref().child('rooms/').push({ roomName: roomName, password: password, user: userRedux.name })
  }

  const Onlogout = async () => {
    const nameuser = userRedux.name
    const key = userRedux.genkey
    dispatch(setLogout())
    navigation.navigate("ChitChat")
    setModalVisibleLogout(!modalVisibleLogout)
    await rootRef.ref('users/' + key).remove()
  }

  const checkStatus = () => {
    if (userRedux.status === false) {
      navigation.navigate("ChitChat")
    }
  }

  return (
    <View style={styles.container}>
      <Modal
        animationType="none"
        transparent={true}
        visible={modalVisibleCreateRoom}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : undefined} style={{ flex: 1 }}>
          <View style={styles.centeredView}>

            <View style={styles.modalView}>

              <TouchableOpacity onPress={() => setModalVisibleCreateRoom(!modalVisibleCreateRoom)} >
                <View style={{ alignItems: 'flex-end' }} lightColor="#FFFFFF" darkColor="#FFFFFF">
                  <AntDesign name="close" size={24} color="black" />
                </View>
              </TouchableOpacity>
              <View style={{ alignItems: "center", marginBottom: 30 }} lightColor="#FFFFFF" darkColor="#FFFFFF">
                <Text style={styles.modalText}>Create Room</Text>
              </View>

              <View style={{ marginTop: 30 }} lightColor="#FFFFFF" darkColor="#FFFFFF">
                <Text style={{ color: "#8F8888" }}>Name</Text>
                <TextInput onChangeText={(e) => setRoomName(e)} style={{ borderBottomColor: "#DEDEDE", borderBottomWidth: 1 }} placeholder="Room name" />
              </View>

              <View style={{ marginTop: 15 }} lightColor="#FFFFFF" darkColor="#FFFFFF">
                <Text style={{ color: "#8F8888" }}>Password</Text>
                <TextInput onChangeText={(e) => setPassword(e)} style={{ borderBottomColor: "#DEDEDE", borderBottomWidth: 1 }} autoCapitalize='none' secureTextEntry={true} placeholder="********" />
              </View>


              <View style={{ flexDirection: 'row', marginTop: 37.5 }} lightColor="#FFFFFF" darkColor="#FFFFFF">
                <View style={{ width: 230, marginLeft: 16 }} lightColor="#FFFFFF" darkColor="#FFFFFF">
                  <TouchableOpacity
                    style={{ ...styles.openButton, backgroundColor: "#8F8888" }}
                    onPress={OnCreateRoomNew}>
                    <Text style={styles.textStyle}>ACCEPT</Text>
                  </TouchableOpacity>
                </View>

              </View>

            </View>
          </View>
        </KeyboardAvoidingView>
      </Modal>


      <Modal
        animationType="none"
        transparent={true}
        visible={modalVisibleLogout}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>

          <View style={styles.modalView} >

            <View style={{ alignItems: "center" }} lightColor="#FFFFFF" darkColor="#FFFFFF">
              <Text style={styles.modalText}>Logout</Text>
            </View>

            <View style={{ marginTop: 23, justifyContent: "center", alignItems: "center" }} lightColor="#FFFFFF" darkColor="#FFFFFF">
              <FontAwesome5 name="check-circle" size={80} color="#00AF80" />
            </View>

            <View style={{ flexDirection: 'row', marginTop: 29 }} lightColor="#FFFFFF" darkColor="#FFFFFF">
              <View style={{ width: 230, marginLeft: 16 }} lightColor="#FFFFFF" darkColor="#FFFFFF">
                <TouchableOpacity
                  style={{ ...styles.openButton, backgroundColor: "#8F8888" }}
                  onPress={Onlogout}>
                  <Text style={styles.textStyle}>SUBMIT</Text>
                </TouchableOpacity>
              </View>

            </View>

          </View>
        </View>
      </Modal>

      <View style={{ backgroundColor: "#4D4D4D", width: '100%', maxHeight: 300, flex : 1, justifyContent: "center", alignItems: "center" }}>
        <View style={{ width: 121, height: 121, marginTop: 100, borderRadius: 100, justifyContent: "center", alignItems: "center" }} lightColor="#FFFFFF" darkColor="#000000">
          {/* <Text style={{ fontWeight: 'bold', fontSize: 56 }}>A</Text> */}
          <TextAvatar
            backgroundColor={'#000000'}
            textColor={'#FFFFFF'}
            size={121}
            type={'circle'} // optional
          >{userRedux.name}</TextAvatar>
        </View>
        <View style={{ backgroundColor: "#4D4D4D", marginTop: 6 }}>
          <Text numberOfLines={1} style={{ fontSize: 28, color: "#FFFFFF" }}>{userRedux.name}</Text>
        </View>
      </View>

      <View style={{ flex : 1,height: 529, width: '100%', alignItems: "center" }} lightColor="#F5F5F5" darkColor="#000000">
        <TouchableOpacity onPress={() => setModalVisibleCreateRoom(true)} style={{ alignItems: "center", maxWidth: 365, width: '100%' }}>
          <View style={{ width: "100%", height: 56, backgroundColor: "#FFFFFF", alignItems: "center", marginTop: 18, flexDirection: 'row' }}>
            <View style={{ marginStart: 20, marginRight: 30, }} lightColor="rgba(0, 0, 0, 0)" darkColor="rgba(0, 0, 0, 0)">
              <SimpleLineIcons name="plus" size={24} color="#000000" />
            </View>
            <View style={{ flex: 1 }} lightColor="rgba(0, 0, 0, 0)" darkColor="rgba(0, 0, 0, 0)">
              <Text lightColor="#000000" darkColor="#000000">Create Room</Text>
            </View>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => setModalVisibleLogout(true)} style={{ alignItems: "center", maxWidth: 365, width: '100%' }}>
          <View style={{ width: '100%', height: 56, backgroundColor: "#FFFFFF", alignItems: "center", marginTop: 18, flexDirection: 'row' }}>
            <View style={{ marginStart: 20, marginRight: 30 }} lightColor="rgba(0, 0, 0, 0)" darkColor="rgba(0, 0, 0, 0)">
              <AntDesign name="logout" size={24} color="#000000" />
            </View>
            <View style={{ flex: 1 }} lightColor="rgba(0, 0, 0, 0)" darkColor="rgba(0, 0, 0, 0)">
              <Text lightColor="#000000" darkColor="#000000">Logout</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>


    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',

    backgroundColor: '#000000'

  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    // alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 10,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 15,
    textAlign: "center",
    color: "#4D4D4D"
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 55,
    backgroundColor: 'rgba(0, 0, 0, 0.52)',
  },
});
