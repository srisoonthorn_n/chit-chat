import React, { useEffect, useState, createRef, useRef } from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, TextInput, PermissionsAndroid, Image, Alert, DrawerLayoutAndroid, Button, FlatList, KeyboardAvoidingView, Platform, } from 'react-native';
import { AntDesign, Ionicons, Entypo, Feather, MaterialCommunityIcons } from '@expo/vector-icons';
import { View, Text } from '../components/Themed';
import { useNavigation } from '@react-navigation/native';

import io from 'socket.io-client'
import { useSelector, useDispatch } from 'react-redux';
import Messages from '../components/Messages';
import { GiftedChat } from 'react-native-gifted-chat'
import { storage } from '../firebase/firebase';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import uuid4 from 'react-native-uuid'
import { setOneRoom, setListUser } from '../Reducer/UserReducer';
import { ProgressBar, Colors } from 'react-native-paper';



let socket: any
var scrollView: any


const ChatScreen = ({ navigation, route }) => {

  const userRedux = useSelector(redux => redux.User)

  // const RoomRudux = useSelector(redux => redux.Room)
  // const oneRoomRedux = useSelector(redux => redux.OneRoomReducer)

  const [message, setMessage] = useState("");


  const [allmessages, setAllMessages] = useState([]);



  const nameRoom = route.params.item.roomName
  const indexRoom = route.params.index

  // const nameRoom = route.params.item

  // console.log("indexRoom", indexRoom);


  const dispatch = useDispatch()

  const OneRoom = () => {
    dispatch(setOneRoom(nameRoom, indexRoom))
  }
  const ListUsers = (users) => {
    dispatch(setListUser(users))
  }

  const checkStatus = () => {
    if (userRedux.status === false) {
      navigation.navigate("ChitChat")
    }
  }


  const [image, setImage] = useState("");
  const [video, setVideo] = useState("");
  const [imageName, setImageName] = useState("");
  const [progress, setProgress] = useState(0);
  const [url, setUrl] = useState("");
  const [error, setError] = useState("");
  const [typeFile, setTypeFile] = useState("");
  const [progressBar, setProgressBar] = useState(false);










  // const Endpoint = 'http://18.222.187.106:9000';
  const Endpoint = 'http://192.168.1.27:9000';
  

  useEffect(() => {
    checkStatus()
    OneRoom()

    const { roomName, name } = userRedux;
    socket = io(Endpoint, {
      secure: true,
      transports: ['websocket'],
    });

    socket.emit('join', { nameRoom, name }, () => {
    });

    socket.on('roomUser', ({ users, nameRoom }) => {
      ListUsers(users)
    })

    socket.on('message', (message: any) => {
      setAllMessages(allmessagesTemp => [...allmessagesTemp, message])
    })

    return () => {
      socket.emit('disconnect');
      socket.close();
      socket.off();
    }
  }, [])



  const handleChangeText = (e: string) => {
    setMessage(e)
  }

  // useEffect(() => {

  //   socket.on('message', (message: any) => {
  //     setAllMessages([...allmessages, message])
  //   })
  // }, [])



  const sendMessage = () => {
    if (message) {
      socket.emit('sendMessage', { user: userRedux.name, type: 'message', data: message, fileExtension: 'text' });
      setMessage("")
    }
    else if (image) {
      if (typeFile == 'jpg') {
        socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'image' });
        setImage("");

      } else if (typeFile == 'mp4') {
        socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'video' });
        setImage("");


      } else if (typeFile == 'mov') {
        socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'video' });
        setImage("");


      } else if (typeFile == 'png') {
        socket.emit('sendMessage', { user: userRedux.name, type: 'file', data: url, fileExtension: 'image' });
        setImage("");

      }
      return setImage("");
    }
  }

  const handlePhotoTake = async () => {
    try {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

      console.log("Status Camara : ", status)

      if (status !== 'granted') {
        return Alert.alert(
          "Permissions denied!",
          `You don't have the access to the camera roll!`,
        )
      }
      _pickImage()

    } catch (e) {
      console.warn(e)
    }


  }

  // const requestCameraPermission = async () => {
  //   try {
  //     const granted = await PermissionsAndroid.request(
  //       PermissionsAndroid.PERMISSIONS.CAMERA,
  //       {
  //         title: "Cool Photo App Camera Permission",
  //         message:
  //           "Cool Photo App needs access to your camera " +
  //           "so you can take awesome pictures.",
  //         buttonNeutral: "Ask Me Later",
  //         buttonNegative: "Cancel",
  //         buttonPositive: "OK"
  //       }
  //     );
  //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //       _pickImage()
  //       console.log("You can use the camera");
  //     } else {
  //       console.log("Camera permission denied");
  //     }
  //   } catch (err) {
  //     console.warn(err);
  //   }
  // };



  const _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });
      if (!result.cancelled) {
        var uuid = uuid4();
        const fileExtension = result.uri.split('.').pop();
        const filename = `${uuid}.${fileExtension}`;

        console.log("filename : ", filename)

        setTypeFile(fileExtension)
        setImage(result.uri)
        setImageName(filename)
        uploadImage(result.uri, filename)
          .then(() => {
            Alert.alert("Success");
            setUrl("");
          })
        console.log(result.uri)
      }

      console.log(result);
    } catch (E) {
      console.log(E);
    }
  };

  const uploadImage = async (uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();

    const uploadTask = storage.ref(`images/${imageName}`).put(blob);
    uploadTask.on(
      "state_changed",
      snapshot => {
        const progress = Math.round(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(progress);
        setProgressBar(true)
      },
      error => {
        setError(error);
      },
      () => {
        storage
          .ref("images")
          .child(imageName)
          .getDownloadURL()
          .then(url => {
            setUrl(url);
            setProgress(0);
            setProgressBar(false)
          });
      }
    );
  }





  // console.log("message", message)

  // console.log("Item Props", nameRoom)

  // console.log("message comeback", messages.user)

  // console.log("Showw All Messages", url)

  // console.log("One Room", oneRoomRedux.room)

  // console.log("Type File", typeFile)
  // console.log("userList", userList)

  // const scrollToBottom= ()=>{
  //   const bottomOfList =  listHeight - scrollViewHeight
  //   console.log('scrollToBottom');
  //   setScrollView(scrollView.scrollTo({ y: bottomOfList }))
  // }


  return (

    <View style={styles.container}>

      <ScrollView style={{ backgroundColor: "#F2F2F2", flex: 1 }}
        ref={ref => { scrollView = ref }}
        showsVerticalScrollIndicator={false}
        onContentSizeChange={() => scrollView.scrollToEnd({ animated: true })}>
        <View style={{ marginLeft: 16, marginRight: 16, marginTop: 24, marginBottom: 24, }}>
          {
            allmessages.map((item, index) => {
              return (
                <View key={index} style={{ backgroundColor: "#F2F2F2" }}>
                  <Messages message={item} name={userRedux.name} index={index} allmessages={allmessages} />
                </View>

              )
            })
          }

        </View>


      </ScrollView>
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : undefined} keyboardVerticalOffset={64}>

        {
          progress === 0 ?
            <View></View>
            :
            <View style={{ backgroundColor: "#F2F2F2", justifyContent: "center"  , alignItems: "center"}}>
              <ProgressBar progress={progress} indeterminate={true} visible={progressBar} color={Colors.red800} style={{ width: 300 }} />
              <Text>{progress}%</Text>
            </View>

        }
        {
          image == "" ?
            <View style={{ alignItems: "flex-end", width: '100%', height: 55 }}>
              <View style={{ flex: 1, width: "100%", justifyContent: "center", alignItems: "center", flexDirection: "row" }} lightColor="#E6E6E6" darkColor="#000000" >
                <View style={{ alignItems: "flex-start", marginLeft: 16, }} lightColor="#E6E6E6" darkColor="#000000">
                  <TouchableOpacity onPress={handlePhotoTake} style={{ flex: 1, justifyContent: "center" }} >
                    <Feather name="paperclip" size={24} color="#555555" />
                  </TouchableOpacity>
                </View>

                <View style={{ flex: 1, marginLeft: 12, marginRight: 12, height: 32 }} lightColor="#FFFFFF" darkColor="#555555">
                  <TextInput value={message} onChangeText={handleChangeText} placeholder="Aaa..." style={{ margin: 6 }} />
                </View>

                <View style={{ alignItems: "flex-end", marginRight: 8, width: 35 }} lightColor="#E6E6E6" darkColor="#000000">
                  <TouchableOpacity onPress={sendMessage} style={{ height: '100%', width: '100%', justifyContent: "center" }} >
                    <Feather name="send" size={24} color="#555555" />
                  </TouchableOpacity>
                </View>
              </View>

            </View>
            :
            <View style={{ justifyContent: "center", alignItems: "center", height: 80, flexDirection: "row" }} lightColor="#E6E6E6" darkColor="#000000">

              <View style={{ marginLeft: 50, flex: 1, alignItems: "center" }}>
                <Image source={{ uri: image }} style={{ resizeMode: "contain", margin: 10, width: 50, height: '75%' }} />
              </View>
              <View style={{ alignItems: "flex-end", marginLeft: 8, marginRight: 16 }} lightColor="#E6E6E6" darkColor="#000000">
                <TouchableOpacity onPress={sendMessage} style={{height : "100%" ,width : "100%" ,justifyContent : "center"}} >
                  <Feather name="send" size={24} color="#555555" />
                </TouchableOpacity>
              </View>
            </View>

        }
      </KeyboardAvoidingView>
    </View >

  );
}
export default ChatScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});



      // <View style={{ height: 60, width: '100%', marginTop: 30, alignItems: "flex-start" }}>
      //   <View style={{ flex: 1, width: "100%", justifyContent: "center", alignItems: "center", flexDirection: "row" }} lightColor="#FFFFFF" darkColor="#000000" >
      //     <View style={{ flex: 1, alignItems: "flex-start", marginLeft: 16 }} lightColor="#FFFFFF" darkColor="#000000">
      //       <TouchableOpacity style={{ width: 80 }} onPress={() => navigation.navigate("Main")}>
      //         <Ionicons name="ios-arrow-back" size={24} color="#555555" />
      //       </TouchableOpacity>
      //     </View>



      //     <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }} lightColor="#FFFFFF" darkColor="#000000">
      //       <Text style={{ fontSize: 20, fontWeight: "bold", fontFamily: "roboto-medium" }} lightColor="#000000" darkColor="#FFFFFF">{nameRoom}</Text>
      //     </View>


      //     <View style={{ flex: 1, alignItems: "flex-end", marginRight: 16 }} lightColor="#FFFFFF" darkColor="#000000">
      //       <TouchableOpacity style={{ width: 80, alignItems: "flex-end" }} onPress={() => navigation.openDrawer()}>
      //         <Entypo name="menu" size={24} color="#555555" />
      //       </TouchableOpacity>
      //     </View>
      //   </View>
      // </View> 