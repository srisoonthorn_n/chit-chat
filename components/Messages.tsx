import React, { useState } from 'react';
import { View, Text } from './Themed';
import { AntDesign } from '@expo/vector-icons';
import { Image, Linking, TouchableOpacity } from 'react-native';
import { Video } from 'expo-av';
import TypeMessage from './TypeMessage';



const Messages = ({ message: { user, data, type, fileExtension }, name, index, allmessages }) => {

    let isSentByCurrentUser = false;

    const trimmedName = name.trim().toLowerCase();
    const trimmedUser = user.trim().toLowerCase();
    const [fileVedio, setFileVedio] = useState('');

    const checkUser = allmessages[index]?.user;
    const checkSendUser = allmessages[index - 1]?.user

    if (trimmedUser == trimmedName) {
        isSentByCurrentUser = true;
    }

    const onCheckUser = (fileExtension) => {

        if (fileExtension == "image") {
            return (
                <View style={{ width: '100%', maxWidth: 310, height: 300, justifyContent: "center", alignItems: "center", backgroundColor: "#E2FFFF", marginRight: 35, }}>
                    <Image source={{ uri: data }} style={{ resizeMode: "contain", width: '80%', height: '80%' }} />
                </View>
            )
        } else if (fileExtension == "video") {
            return (
                <View style={{ width: '100%', maxWidth: 310, height: 310, justifyContent: "center", alignItems: "center", backgroundColor: "#E2FFFF", marginRight: 35, }}>
                    <Video
                        source={{ uri: data }}
                        rate={1.0}
                        volume={1.0}
                        isMuted={false}
                        resizeMode="contain"
                        shouldPlay={false}
                        isLooping={false}
                        useNativeControls
                        style={{ width: '80%', height: '80%' }}
                    />

                </View >
            )

        }
    }

    const onCheckOther = (fileExtension) => {

        if (fileExtension == "image") {
            return (
                <View style={{ width: '100%', maxWidth: 310, height: 300, justifyContent: "center", alignItems: "center", marginStart: 35, backgroundColor: "#FFFFFF" }}>
                    <Image source={{ uri: data }} style={{ resizeMode: "contain", width: '80%', height: '80%' }} />
                </View>
            )
        } else if (fileExtension == "video") {
            return (
                <View style={{ width: '100%', maxWidth: 310, height: 310, justifyContent: "center", alignItems: "center", marginStart : 35, backgroundColor: "#FFFFFF" }}>
                    <Video
                        source={{ uri: data }}
                        rate={1.0}
                        volume={1.0}
                        isMuted={false}
                        resizeMode="contain"
                        shouldPlay={false}
                        isLooping={false}
                        useNativeControls
                        style={{ width: '80%', height: '80%' }}
                    />

                </View >
            )

        }
    }

    // console.log("is Sent By CurrentUser", isSentByCurrentUser)
    // console.log("type", fileExtension)
    // console.log("data", data)
    // console.log("index - 1", index - 1)
    // console.log("allmessages", checkUser)
    // console.log("checkSendUser", checkSendUser)
    return (
        isSentByCurrentUser
            ? (
                <View style={{ alignItems: "flex-end", backgroundColor: "#F2F2F2", marginBottom: 5, marginStart: 76 }}>
                    {
                        checkUser == checkSendUser ?
                            <View style={{ width: "100%", maxWidth: 50, marginEnd: 35, flexDirection: "row", backgroundColor: "#F2F2F2", }}>
                            </View>
                            :
                            <View style={{ width: '100%', maxWidth: 50, marginStart: 8, marginEnd: 35, marginBottom: 8, flexDirection: "row", backgroundColor: "#F2F2F2", }}>
                                <View style={{ backgroundColor: "#F2F2F2", width: 50, alignItems: "center" }} >
                                    <Text style={{ color: "#000000" }}>{name}</Text>
                                </View>
                                <View style={{ alignItems: "center", justifyContent: "center", backgroundColor: "#999999", borderRadius: 90, height: 24, width: 24 }}>
                                    <AntDesign name="user" size={18} color="black" />
                                </View>

                            </View>
                    }

                    {
                        type == "file" ?

                            <View style={{ width: "100%", backgroundColor: "#F2F2F2", alignItems: "flex-end" }}>
                                {
                                    onCheckUser(fileExtension)
                                }
                            </View>

                            :
                            <View style={{ backgroundColor: "#E2FFFF", maxWidth: 310, minWidth: 0, padding: 8, justifyContent: "center", marginEnd: 35 }}>
                                <Text style={{ color: "#666666", fontSize: 14 }}>{data}</Text>
                            </View>
                    }

                </View>
            )
            : (
                <View style={{ alignItems: "flex-start", backgroundColor: "#F2F2F2", marginBottom: 5 }}>

                    {
                        checkUser == checkSendUser ?
                            <View style={{ width: "100%", maxWidth: 50, marginEnd: 35, marginStart: 35, flexDirection: "row", backgroundColor: "#F2F2F2", }}>

                            </View>
                            :
                            <View style={{ width: '100%', maxWidth: 50, marginEnd: 35, marginStart: 10, marginBottom: 8, flexDirection: "row", backgroundColor: "#F2F2F2", }}>
                                <View style={{ alignItems: "center", justifyContent: "center", backgroundColor: "#999999", borderRadius: 90, height: 24, width: 24 }}>
                                    <AntDesign name="user" size={18} color="black" />
                                </View>
                                <View style={{ backgroundColor: "#F2F2F2", width: 50, alignItems: "center" }}>
                                    <Text style={{ color: "#000000" }}>{user}</Text>
                                </View>

                            </View>
                    }

                    {
                        type == "file" ?

                            <View style={{ width: "100%", backgroundColor: "#F2F2F2", alignItems: "flex-start" }}>
                                {
                                    onCheckOther(fileExtension)
                                }

                            </View>
                            :
                            <View style={{ backgroundColor: "#FFFFFF",maxWidth : 310, minWidth : 0 ,padding: 8, marginStart: 35, justifyContent: "center" }}>
                                <Text style={{ color: "#666666", fontSize: 14 }}>{data}</Text>
                            </View>
                    }

                </View>

            )
    );
}

export default Messages;





//  {
//                                     typeFile == "jpg" ?
//                                         <Image source={{ uri: data }} style={{ width: '80%', height: '80%' }} />
//                                         :
//                                         <Video
//                                             source={{ uri: data }}
//                                             rate={1.0}
//                                             volume={1.0}
//                                             isMuted={false}
//                                             resizeMode="cover"
//                                             shouldPlay={false}
//                                             isLooping={false}
//                                             useNativeControls
//                                             style={{ width: '80%', height: '80%' }}
//                                         />

//                                 } 




{/* <View style={{ marginBottom: 5, backgroundColor: "#F2F2F2", flexDirection: "row" }}>

{
    type == "file" ?

        (
            <View style={{ marginRight: 26, backgroundColor: "#F2F2F2" }} >
                {onCheckUser(fileExtension)}
            </View>


        )

        :
        <View style={{ alignItems: "flex-end" , flex : 1}} lightColor="#F2F2F2" darkColor="#F2F2F2">
            <View style={{ minHeight: 31, maxHeight: 57, width: '100%', maxWidth: 240, backgroundColor: "#E2FFFF", marginLeft: 16, marginRight: 16, padding: 10 }}>
                <Text style={{ fontSize: 12, color: "#000000" }}>{data}</Text>
            </View>
        </View>
}

{
    checkUser == checkSendUser ?
        <View style={{ width : 80,alignItems: "center", justifyContent: "flex-end",backgroundColor: "#F2F2F2"}}>

        </View>
        :
        <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end", backgroundColor: "#F2F2F2" }}>
            <View style={{ backgroundColor: "#F2F2F2" }}>
                <Text style={{ color: "#000000" }}>{name}</Text>
            </View>
            <View style={{ backgroundColor: "#999999", borderRadius: 90, marginStart: 8, height: 30, width: 30, justifyContent: "center", alignItems: "center" }}>
                <AntDesign name="user" size={18} color="black" />
            </View>
        </View>
}



</View> */}




{/* <View style={{ marginBottom: 5, backgroundColor: "#F2F2F2", marginRight: 26, flexDirection: 'row' }}>
{
    checkUser == checkSendUser ?
        <View></View>
        :
        <View style={{ flexDirection: "row", alignItems: "center", backgroundColor: "#F2F2F2" }}>
            <View style={{ backgroundColor: "#999999", borderRadius: 90, marginEnd: 8, height: 30, width: 30, justifyContent: "center", alignItems: "center" }}>
                <AntDesign name="user" size={18} color="black" />
            </View>
            <View style={{ backgroundColor: "#F2F2F2" }}>
                <Text lightColor="#000000" darkColor="#000000">{user} </Text>
            </View>
        </View>
}


{
    type == "file" ?
        onCheckOther(fileExtension)
        :
        <View style={{ minHeight: 31, maxHeight: 57, width: '100%', maxWidth: 240, marginLeft: 16, marginRight: 16, padding: 10 }}>
            <Text style={{ fontSize: 12 }}>{data}</Text>
        </View>
}



</View> */}