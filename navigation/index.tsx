import { NavigationContainer, DefaultTheme, DarkTheme, DrawerActions, useNavigation } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName, TouchableOpacity } from 'react-native';

import NotFoundScreen from '../screens/NotFoundScreen';
import { RootStackParamList, DrawerParamList } from '../types';
import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';
import ChitChatLogoScreen from '../screens/ChitChatLogoScreen';
import ChatScreen from '../screens/ChatScreen';
import MemberListScreen from '../screens/MemberListScreen';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import { MaterialCommunityIcons, Feather, Entypo, Ionicons } from '@expo/vector-icons';
import { createStore } from 'redux';

import { Provider, useSelector } from 'react-redux';
import { rootReducer } from '../Reducer';
import Messages from '../components/Messages';
import { View, Text } from '../components/Themed';

const store = createStore(rootReducer)



// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <Provider store={store}>
      <NavigationContainer
        linking={LinkingConfiguration}
        theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
        <RootNavigator />
      </NavigationContainer>
    </Provider>

  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator<RootStackParamList>();



function RootNavigator() {

  const oneRoomRedux = useSelector(redux => redux.OneRoomReducer)
  return (
    <Stack.Navigator >
      <Stack.Screen name="ChitChat" component={ChitChatLogoScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Main" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="Chat" component={DrawerNavigator} options={{
        headerTitle: oneRoomRedux.room,
        headerTitleStyle: { alignSelf: 'center' },
        headerLeft: (props) => <ChatContentLeft {...props} />,
        headerRight: () => <ChatContentRight />
      }} />
      <Stack.Screen name="MemberList" component={MemberListScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Messages" component={Messages} options={{ headerShown: false }} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!', headerShown: false }} />
    </Stack.Navigator>
  );
}

const ChatContentRight = () => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity style={{ width: 80, marginRight: 20, alignItems: "flex-end" }} onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}  >
      <Entypo name="menu" size={24} color="#555555" />
    </TouchableOpacity>
  )
}

const ChatContentLeft = props => {

  return (
    <TouchableOpacity style={{ width: 80, marginLeft: 20 }} onPress={() => props.navigation.goBack()} {...props}>
      <Ionicons name="ios-arrow-back" size={24} color="#555555" />
    </TouchableOpacity>
  )
}

const Drawer = createDrawerNavigator<DrawerParamList>();

const MemberOrNot = (props) => {

  const userRedux = useSelector(redux => redux.User);
  const index = useSelector(redux => redux.OneRoomReducer)
  const Room = useSelector(redux => redux.setDataReducer.roomList[index.index]?.user);

  console.log("index", index)
  console.log("RoomRedux", Room)

  if (Room == userRedux.name) {
    return (
      <DrawerContentScrollView  {...props}>
        <DrawerItem
          label="See Group Member"
          style={{ borderBottomWidth: 1, borderBottomColor: "#707070", }}
          icon={() => <MaterialCommunityIcons name="logout-variant" size={24} color="#707070" />}
          onPress={() => props.navigation.navigate("MemberList")} />

        <DrawerItem
          label="Leave Group"
          style={{ borderBottomWidth: 1, borderBottomColor: "#707070" }}
          icon={() => <Feather name="users" size={24} color="#707070" />}
          onPress={() => props.navigation.navigate("Main")} />
      </DrawerContentScrollView>
    )

  } else {
    return (
      <DrawerContentScrollView  {...props}>
        <DrawerItem
          label="Leave Group"
          style={{ borderBottomWidth: 1, borderBottomColor: "#707070" }}
          icon={() => <Feather name="users" size={24} color="#707070" />}
          onPress={() => props.navigation.navigate("Main")} />
      </DrawerContentScrollView>
    )


  }
}

const DrawerContent = props => {
  return (
    // <DrawerContentScrollView  {...props}>
    //   <DrawerItem
    //     label="See Group Member"
    //     style={{ borderBottomWidth: 1, borderBottomColor: "#707070", }}
    //     icon={() => <MaterialCommunityIcons name="logout-variant" size={24} color="#707070" />}
    //     onPress={() => props.navigation.navigate("MemberList")} />

    //   <DrawerItem
    //     label="Leave Group"
    //     style={{ borderBottomWidth: 1, borderBottomColor: "#707070" }}
    //     icon={() => <Feather name="users" size={24} color="#707070" />}
    //     onPress={() => props.navigation.navigate("Main")} />
    // </DrawerContentScrollView>
    MemberOrNot(props)
  )
}

function DrawerNavigator() {
  return (
    <Drawer.Navigator initialRouteName="Chat" drawerPosition={'right'} screenOptions={{ gestureEnabled: true }} drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen name="Chat" component={ChatScreen} />
    </Drawer.Navigator>
  )
}
