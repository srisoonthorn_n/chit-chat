import { combineReducers } from "redux";
import { UserReducer, RoomReducer  , OneRoomReducer , ListUserReducer , setDataReducer} from "./UserReducer";

export const rootReducer = combineReducers({
    User  : UserReducer,
    Room : RoomReducer,
    OneRoomReducer,
    ListUserReducer,
    setDataReducer
})