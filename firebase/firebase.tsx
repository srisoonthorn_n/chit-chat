import firebase from "firebase"
import 'firebase/storage';



var firebaseConfig = {
  apiKey: "AIzaSyBR1euZMjZit_Ql0ozH9-0fK5lbHPyWHWo",
  authDomain: "chit-chat-app-101ea.firebaseapp.com",
  databaseURL: "https://chit-chat-app-101ea.firebaseio.com",
  projectId: "chit-chat-app-101ea",
  storageBucket: "chit-chat-app-101ea.appspot.com",
  messagingSenderId: "720181355136",
  appId: "1:720181355136:web:adb4ae0b11bef70e0c3c65",
  measurementId: "G-BG8Z13SLSX"
};

// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const fireAuth = firebase.auth();
const storage = firebase.storage();
const rootRef = firebase.database();

export {
  storage, rootRef, fireAuth,  firebase as default
}