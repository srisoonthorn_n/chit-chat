import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { StyleSheet, TouchableOpacity, TextInput } from 'react-native';

import { Text, View } from './Themed';
import { Feather } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';


export default function BottomChatScreen() {
    const navigation = useNavigation();
    return (
        <View style={{ flex: 1, width: "100%", justifyContent: "center", alignItems: "center", flexDirection: "row" }} lightColor="#E6E6E6" darkColor="#000000" >
            <View style={{  alignItems: "flex-start", marginLeft: 16 }}  lightColor="#E6E6E6" darkColor="#000000">
                <TouchableOpacity >
                    <Feather name="paperclip" size={24} color="#555555" />
                </TouchableOpacity>
            </View>



            <View style={{ flex: 1, marginLeft : 12 , marginRight  : 12  ,height : 32}}  lightColor="#FFFFFF" darkColor="#555555">
                <TextInput placeholder="Aaa..." style={{margin : 6}} />            
                </View>


            <View style={{  alignItems: "flex-end", marginRight: 16 }}  lightColor="#E6E6E6" darkColor="#000000">
                <TouchableOpacity>
                <Feather name="send" size={24} color="#555555" />
                </TouchableOpacity>
            </View>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    developmentModeText: {
        marginBottom: 20,
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        lineHeight: 24,
        textAlign: 'center',
    },
    helpContainer: {
        marginTop: 15,
        marginHorizontal: 20,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        textAlign: 'center',
    },
});
