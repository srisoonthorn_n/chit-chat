export type RootStackParamList = {
  ChitChat : undefined;
  Main: undefined;
  Chat : {
    DATA : string
  };
  MemberList : undefined;
  NotFound: undefined;
  Drawer : undefined;
  RoomList : {
    DATA : string
  }
  Messages : undefined;
};

export type DrawerParamList = {
  MemberList : undefined;
  Chat : undefined;
}

export type BottomTabParamList = {
  Chat : undefined;
  Room: undefined;
  Profile: undefined;
};

export type RoomListParam = {
  RoomListScreen: {
    roomName : string
  };
};

export type ProfileParam = {
  ProfileScreen: undefined;
};

export type ChitChatParam = {
  ChitChat: undefined;
};

export type ChatParam = {
  chat: {
    roomName : string
  };
};
