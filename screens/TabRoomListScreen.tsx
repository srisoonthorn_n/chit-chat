import * as React from 'react';
import { StyleSheet, TextInput, View, Alert } from 'react-native';
import { Button } from 'react-native-material-ui';
import { FontAwesome } from '@expo/vector-icons';
import RoomListScreen from '../components/RoomListScreen';


export default function TabRoomListScreen() {

  const [search, setSearch] = React.useState("");
  const [OnPressSearch , setOnPressSearch] = React.useState("")

  

  console.log("search",search)
  console.log("OnPressSearch",OnPressSearch)

  return (
    <View style={styles.container}>
      <View style={{ backgroundColor: "#E6E6E6", width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

        <View style={{ flex: 1, height: 32, backgroundColor: '#FFFFFF', marginStart: 10, borderRadius: 2, flexDirection: 'row'  , marginEnd : 10}}>
          <View style={{alignItems : "center"}}>
            <FontAwesome name="search" size={22} color="#707070" style={{ marginStart: 10, padding: 5 }} />
          </View>
          <View style={{ flex: 1 , justifyContent: "center"  }}>
            <TextInput
              placeholder="Search"
              value={search}
              onChangeText={(event: any) => setSearch(event)}
            />
          </View>
        </View>
        {/* <View>
          <Button onPress={(value) =>onClickSearch(value)} primary text="Search" />
        </View> */}

      </View>

      <View style={{ flex: 1, width: '100%', height: '100%', justifyContent: "center", alignContent: "center" }}>
        <RoomListScreen search={search}/>
      </View>




    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
