import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import * as Font from 'expo-font';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import { AppLoading } from 'expo';
import { useSelector } from 'react-redux';
import ChitChatLogoScreen from './screens/ChitChatLogoScreen';


const getFonts = () => Font.loadAsync({
  'roboto-regular': require('./assets/fonts/Roboto-Regular.ttf'),
  'roboto-italic': require('./assets/fonts/Roboto-Italic.ttf'),
  'roboto-medium': require('./assets/fonts/Roboto-Medium.ttf'),


})




export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (fontsLoaded) {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar />
      </SafeAreaProvider>
    );
  } else {
    return <AppLoading
      startAsync={getFonts}
      onFinish={() => setFontsLoaded(true)} />

  }
}
