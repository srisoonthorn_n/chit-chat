import * as React from 'react';
import { StyleSheet, TextInput, View, Image, Text, Button, Modal, TouchableHighlight, Alert, KeyboardAvoidingView, Dimensions, Platform } from 'react-native';
import Navigation from '../navigation';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { setUser } from '../Reducer/UserReducer';
import io from "socket.io-client";
import { Header } from '@react-navigation/stack';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { fireAuth } from '../firebase/firebase';
import { rootRef } from '../firebase/firebase';



export default function ChitChatLogoScreen() {

  const [value, setValue] = React.useState("");
  const [name, setName] = React.useState("");
  const [status, setStatus] = React.useState(false);
  const navigation = useNavigation();
  const behavior = Platform.OS === 'ios' ? 'position' : 'position';
  const dispatch = useDispatch()
  const [userData, setUserData] = React.useState([]);

  const [checkuserName, setCheckUserName] = React.useState(false);
  const [userDuplicate, setUserDuplicate] = React.useState(false);
  const [key, setKey] = React.useState(1)
  const [showData, setShowData] = React.useState([])
  // const navtigation = useNavigation();

  console.log("showData", showData)

  // console.log("UserData", userData.forEach(function (index){
  //   console.log("Indexxxx",index)
  // }))


  React.useEffect(() => {
    getKey()
    const userRef = rootRef.ref('users');
    const onLoadingListerer = userRef.on('value', snapshot => {
      setUserData([]);
      snapshot.forEach(function (childSnapshot) {
        setUserData(DATA => [...DATA, childSnapshot.val()]);
        childSnapshot.forEach(function (DataChild) {
          setShowData([DataChild.val()]);
        })

      });
    });
    return () => {
      userRef.off('value', onLoadingListerer)
    }
  }, [])

  const getKey = () => {
    var RandomNumber = Math.floor(Math.random() * 500) + 1;
    setKey(RandomNumber)
  }

  const onStart = async () => {
    if (name == "") {
      setCheckUserName(true)
      setUserDuplicate(false)
    } else if (name !== "") {
      if (showData.find((user) => user.name == name)) {
        setUserDuplicate(true)
        setCheckUserName(false)
      } else {
        const Changestatus = true
        setStatus(Changestatus)
        dispatch(setUser(name, key))
        navigation.navigate("Main")
        setName("");
        setCheckUserName(false)
        setUserDuplicate(false);
        await rootRef.ref('users/' + key).push({ name: name })

      }
    }
  }

  return (
    <KeyboardAvoidingView behavior={behavior} >
      <View style={styles.container}>
        <View style={{ height: 450, justifyContent: 'center', alignItems: 'center' }}>
          <View style={{ marginBottom: 4 }}>
            <Image
              style={{ width: 225, height: 225 }}
              source={require('../assets/images/logo.png')}
            />
          </View>
          <View>
            <Text style={{ fontFamily: "roboto-italic", fontSize: 36, color: "#F5511E" }}>Chit Chat</Text>
          </View>
        </View>


        <View style={{ marginStart: 102, marginEnd: 102, marginBottom: 24, width: '100%', maxWidth: 250 }}>
          <View style={{ marginBottom: 5 }}>
            <Text style={{ color: "#8F8888" }}>Username</Text>
          </View>
          <View style={{ padding: 5, borderColor: "#BEBEBE", borderWidth: 1 }} >
            <TextInput value={name} onChangeText={(e) => setName(e)} style={{ color: "#F5511E" }} placeholder="Username" />
          </View>
          {
            checkuserName == true ?
              <View style={{ justifyContent: "center", alignItems: "center", marginTop: 5 }}>
                <Text style={{ fontSize: 12, color: "#C10006" }}>Please enter username.</Text>
              </View>
              :
              <View></View>
          }
          {
            userDuplicate == true ?
              <View style={{ justifyContent: "center", alignItems: "center", marginTop: 5 }}>
                <Text style={{ fontSize: 12, color: "#C10006" }}>Username is not available.</Text>
              </View>
              :
              <View></View>
          }
        </View>



        <View style={styles.viewButton}>
          {/* <Button
        title="Start Chat"
        color="#707070"
      /> */}
          <TouchableOpacity onPress={onStart} style={{ backgroundColor: "#707070", justifyContent: "center", alignItems: "center", padding: 10, borderRadius: 2 }}>
            <Text style={{ color: "#FFFFFF" }}>START CHAT</Text>
          </TouchableOpacity>

        </View>
      </View >
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 50
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  viewButton: {
    width: '100%',
    maxWidth: 250,
    marginStart: 102,
    marginEnd: 102
  }
});
