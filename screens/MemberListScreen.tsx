import * as WebBrowser from 'expo-web-browser';
import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, Button, ScrollView, Modal, Alert, TouchableHighlight, TextInput, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Ionicons, Entypo } from '@expo/vector-icons';
import { View, Text } from '../components/Themed';
import { MaterialIcons } from '@expo/vector-icons';
import { useSelector } from 'react-redux';
import io from 'socket.io-client'
import TextAvatar from 'react-native-text-avatar'


let socket: any

export default function MemberListScreen() {


    const navigation = useNavigation();

    const oneRoomRedux = useSelector(redux => redux.OneRoomReducer)

    const ListUser = useSelector(redux => redux.ListUserReducer.users)


    console.log("ListUser", ListUser)


    return (
        <View>
            <View style={{ height: 55, width: 'auto', marginTop: 20, alignItems: "flex-start" }}>
                <View style={{ flex: 1, width: "100%", justifyContent: "center", alignItems: "center", flexDirection: "row" }} lightColor="#FFFFFF" darkColor="#000000" >
                    <View style={{ flex: 1, alignItems: "flex-start", marginLeft: 16 }} lightColor="#FFFFFF" darkColor="#000000">
                        <TouchableOpacity style={{ width: 80 }} onPress={() => navigation.navigate("Chat")}>
                            <Ionicons name="ios-arrow-back" size={24} color="#555555" />
                        </TouchableOpacity>
                    </View>



                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }} lightColor="#FFFFFF" darkColor="#000000">
                        <Text style={{ fontSize: 20, fontWeight: "bold", alignItems: "center" }} lightColor="#000000" darkColor="#FFFFFF">{oneRoomRedux.room}</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                    </View>
                </View>
            </View>


            <ScrollView style={{ height: '100%', width: '100%', backgroundColor: '#F5F5F5' }}>

                <View style={{ marginRight: 33, marginLeft: 33, marginTop: 48 }} lightColor="#F5F5F5" darkColor="#F5F5F5" >
                    <Text style={{ color: '#8F8888', fontSize: 24, fontWeight: 'bold' }} lightColor="#F5F5F5" darkColor="#F5F5F5">Member</Text>
                </View>



                <View style={{ justifyContent: 'center', marginTop: 10, alignItems: "center", marginLeft: 33, marginRight: 38 }} lightColor="#F5F5F5" darkColor="#F5F5F5">


                    {
                        ListUser.map((itme, index) => {
                            return (
                                <View key={index} style={{ backgroundColor: '#F5F5F5', width: '100%', maxWidth: 500, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center' }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                                    <View style={{ width: 40, height: 40, marginRight: 15, marginLeft: 16, borderRadius: 100, justifyContent: "center", alignItems: "center" }} lightColor="#707070" darkColor="#707070">
                                        <TextAvatar
                                            backgroundColor={'#707070'}
                                            textColor={'#FFFFFF'}
                                            size={40}
                                            type={'circle'} // optional
                                        >{itme.name}</TextAvatar>
                                    </View>
                                    <View style={{ flex: 1 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                                        <Text style={{ fontSize: 16, color: '#000000' }} lightColor="#F5F5F5" darkColor="#F5F5F5">{itme.name}</Text>
                                    </View>
                                    <View style={{ marginRight: 16 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                                        {/* <TouchableOpacity >
                                            <MaterialIcons name="delete" size={24} color="#707070" />
                                        </TouchableOpacity> */}
                                    </View>
                                </View>
                            )
                        })
                    }

                    {/* <View style={{ backgroundColor: '#F5F5F5', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center' }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                        <View style={{ width: 40, height: 40, marginRight: 15, marginLeft: 16, borderRadius: 100, justifyContent: "center", alignItems: "center" }} lightColor="#707070" darkColor="#707070">
                            <Text style={{ color: "#FFFFFF" }}>L</Text>
                        </View>
                        <View style={{ flex: 1 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <Text style={{ fontSize: 16, color: '#000000' }} lightColor="#F5F5F5" darkColor="#F5F5F5">Line item selected</Text>
                        </View>
                        <View style={{ marginRight: 16 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <TouchableOpacity>
                                <MaterialIcons name="delete" size={24} color="#707070" />
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{ backgroundColor: '#F5F5F5', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center' }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                        <View style={{ width: 40, height: 40, marginRight: 15, marginLeft: 16, borderRadius: 100, justifyContent: "center", alignItems: "center" }} lightColor="#707070" darkColor="#707070">
                            <Text style={{ color: "#FFFFFF" }}>L</Text>
                        </View>
                        <View style={{ flex: 1 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <Text style={{ fontSize: 16, color: '#000000' }} lightColor="#F5F5F5" darkColor="#F5F5F5">Line item selected</Text>
                        </View>
                        <View style={{ marginRight: 16 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <TouchableOpacity>
                                <MaterialIcons name="delete" size={24} color="#707070" />
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{ backgroundColor: '#F5F5F5', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center' }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                        <View style={{ width: 40, height: 40, marginRight: 15, marginLeft: 16, borderRadius: 100, justifyContent: "center", alignItems: "center" }} lightColor="#707070" darkColor="#707070">
                            <Text style={{ color: "#FFFFFF" }}>L</Text>
                        </View>
                        <View style={{ flex: 1 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <Text style={{ fontSize: 16, color: '#000000' }} lightColor="#F5F5F5" darkColor="#F5F5F5">Line item selected</Text>
                        </View>
                        <View style={{ marginRight: 16 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <TouchableOpacity>
                                <MaterialIcons name="delete" size={24} color="#707070" />
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{ backgroundColor: '#F5F5F5', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center' }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                        <View style={{ width: 40, height: 40, marginRight: 15, marginLeft: 16, borderRadius: 100, justifyContent: "center", alignItems: "center" }} lightColor="#707070" darkColor="#707070">
                            <Text style={{ color: "#FFFFFF" }}>L</Text>
                        </View>
                        <View style={{ flex: 1 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <Text style={{ fontSize: 16, color: '#000000' }} lightColor="#F5F5F5" darkColor="#F5F5F5">Line item selected</Text>
                        </View>
                        <View style={{ marginRight: 16 }} lightColor="#F5F5F5" darkColor="#F5F5F5">
                            <TouchableOpacity>
                                <MaterialIcons name="delete" size={24} color="#707070" />
                            </TouchableOpacity>
                        </View>
                    </View> */}

                </View>

            </ScrollView>
        </View>


    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    developmentModeText: {
        marginBottom: 20,
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        lineHeight: 24,
        textAlign: 'center',
    },
    helpContainer: {
        marginTop: 15,
        marginHorizontal: 20,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        textAlign: 'center',
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 1.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 10,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 15,
        textAlign: "center"
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 55,
        backgroundColor: 'rgba(0, 0, 0, 0.52)',
    },
});
