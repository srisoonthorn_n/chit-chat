import { Ionicons, FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import { BottomTabParamList, ProfileParam , ChitChatParam, RoomListParam } from '../types';
import TabRoomListScreen from '../screens/TabRoomListScreen';
import TabProfileScreen from '../screens/TabProfileScreen';
import ChitChatLogoScreen from '../screens/ChitChatLogoScreen';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Chat"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
      <BottomTab.Screen
        name="Room"
        component={RoomListNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-chatbubbles" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({ color }) => <IconFontAwesom name="user-circle" color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

function IconFontAwesom(props: { name: string; color: string }) {
  return <FontAwesome size={24} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const RoomListStack = createStackNavigator<RoomListParam>();

function RoomListNavigator() {
  return (
    <RoomListStack.Navigator>
      <RoomListStack.Screen
        name="RoomListScreen"
        component={TabRoomListScreen}
        options={{ headerTitle: 'Chit Chat' ,  headerTitleStyle: { alignSelf: 'center' },headerLeft: ()=>null }}
      />
    </RoomListStack.Navigator>
  );
}

const ProfileStack = createStackNavigator<ProfileParam>();

function ProfileNavigator() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name="ProfileScreen"
        component={TabProfileScreen}
        options={{ headerTitle: 'Chit Chat' ,headerTitleStyle: { alignSelf: 'center' }, headerLeft: ()=>null }}
      />
    </ProfileStack.Navigator>
  );
}

const ChitChat = createStackNavigator<ChitChatParam>();

function ChitChatNavigator() {
  return (
    <ChitChat.Navigator>
      <ChitChat.Screen
        name="ChitChat"
        component={ChitChatLogoScreen}
      />
    </ChitChat.Navigator>
  );
}

