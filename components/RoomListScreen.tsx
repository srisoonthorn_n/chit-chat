import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import { StyleSheet, View, Image, Button, Text, FlatList, Dimensions, TouchableOpacity, TextInput, Alert, Modal, KeyboardAvoidingView, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { FontAwesome, AntDesign } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';
import { rootReducer } from '../Reducer';
import { rootRef, storage } from '../firebase/firebase';
import { setRoom, setOneRoom, setData } from '../Reducer/UserReducer';
import { SafeAreaView } from 'react-native-safe-area-context';
import TextAvatar from 'react-native-text-avatar'



// const DATA = [
//     {

//         roomName: "FootBall"
//     },
//     {

//         roomName: "Game"
//     },
//     {

//         roomName: "Sport"
//     },
// ]


export default function RoomListScreen({ search }) {

    const [modalVisible, setModalVisible] = React.useState(false);
    const navigation = useNavigation();
    const userRedux = useSelector(redux => redux.User)

    const RoomRedux = useSelector(redux => redux.Room)

    const [DATA, setDATA] = React.useState([]);
    const [password, setPassword] = React.useState("");
    const [index, setIndex] = React.useState(0);
    const [item, setitem] = React.useState(0);

    const dispatch = useDispatch()
    // console.log("New Room", RoomRedux)
    // console.log("room Name DATA", DATA)
    // console.log("search", search)
    // console.log("RoomRedux password", DATA)
    console.log("item on state", item)


    // console.log("Room Name Show", userRedux)
    // console.log("User", userRedux)

    // React.useEffect(() => {
    // const { room, name } = userRedux

    // }, [])

    React.useEffect(() => {
        checkStatus()
        const userRef = rootRef.ref('rooms');
        const onLoadingListerer = userRef.on('value', snapshot => {
            setDATA([]);
            snapshot.forEach(function (childSnapshot) {
                setDATA(DATA => [...DATA, childSnapshot.val()]);

            });

        });
        return () => {
            userRef.off('value', onLoadingListerer)
        }
    }, [])

    React.useEffect(() => {
        dispatch(setData(DATA))
    })


    const checkStatus = () => {
        if (userRedux.status === false) {
            navigation.navigate("ChitChat")
        }
    }

    const CheckPassword = ({ item, index }) => {
        const InputPassword = password
        if (search == "") {
            const RoomPassword = DATA[index]
            if (RoomPassword.password === InputPassword) {
                navigation.navigate('Chat', { screen: 'Chat', params: { item, index } })
                setModalVisible(false)
                setPassword("")
            } else {
                Alert.alert("Wrong Password please try again.")
            }
        } else {
            const searchList = search
            let RoomPassword = DATA.filter(value => value.roomName.toLowerCase().match(searchList.toLowerCase()))
            const SelectRoom = RoomPassword[index]
            console.log("RoomPassword", RoomPassword)
            if (SelectRoom.password === InputPassword) {
                navigation.navigate('Chat', { screen: 'Chat', params: { item, index } })
                setModalVisible(false)
                setPassword("")
            } else {
                Alert.alert("Wrong Password please try again.")
            }
        }


    }

    const ClickJoin = ({ item, index }) => {
        navigation.navigate('Chat', { screen: 'Chat', params: { item, index } })
    }





    const { width, height } = Dimensions.get('window');

    // const ModalShow = (modalVisible) => {
    //     console.log("Showwwww " ,modalVisible)
    //     if (modalVisible == true) {
    //         return (

    //         )
    //     }

    // }

    const ShowModal = (item, index) => {
        setModalVisible(true)
        setIndex(item.index)
        console.log('item on function', item)
        setitem(item.item)
    }

    const onSearch = (value) => {
        const listShow = value
        const searchList = search
        return listShow.filter(value => value.roomName.toLowerCase().match(searchList.toLowerCase()))
    }
    const cancel = () => {
        setModalVisible(!modalVisible)
        setPassword("")
    }



    return (

        <View style={{ width: '100%', height: '100%', backgroundColor: '#F5F5F5' }}>

            <Modal animationType="none"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setModalVisible(!modalVisible)
                }}>
                <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : undefined} style={{ flex: 1 }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <TouchableOpacity onPress={() => setModalVisible(!modalVisible)} >
                                <View style={{ alignItems: 'flex-end' }} >
                                    <AntDesign name="close" size={24} color="black" />
                                </View>
                            </TouchableOpacity>

                            <View style={{ alignItems: "center" }} >
                                <Text style={styles.modalText}>Confirm Password</Text>
                            </View>
                            <View style={{ alignItems: "center", marginBottom: 30 }} >
                                <Text style={{ fontSize: 16, color: "#8F8888" }}>Please input password room</Text>
                            </View>
                            <View style={{ alignItems: "center", marginBottom: 30, }} >
                                <TextInput
                                    onChangeText={(e) => setPassword(e)}
                                    value={password}
                                    style={{ borderBottomColor: "#DEDEDE", borderBottomWidth: 1, width: 300, maxWidth: 300, }} autoCapitalize='none' secureTextEntry={true} placeholder="Password" />
                            </View>

                            <View style={{ width: "100%", maxWidth: 350, flexDirection: "row" }}>
                                <View style={{ flex: 1, marginEnd: 20 }}>
                                    <TouchableOpacity style={{ ...styles.openButton, backgroundColor: "#8F8888" }} onPress={cancel}>
                                        <Text style={styles.textStyle}>CANCEL</Text>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, marginStart: 20 }}>
                                    <TouchableOpacity style={{ ...styles.openButton, backgroundColor: "#8F8888" }} onPress={() => CheckPassword({ item, index })}>
                                        <Text style={styles.textStyle}>ACCEPT</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                    </View>
                </KeyboardAvoidingView>

            </Modal>



            <View style={{ justifyContent: "center", alignItems: "flex-start", marginRight: 33, marginLeft: 33, marginTop: 48 }}>
                <Text style={{ color: '#8F8888', fontSize: 24, fontWeight: 'bold' }}># ROOM</Text>
            </View>


            {
                DATA.length == 0 || DATA == null ?

                    <View style={{ flex: 1, width: '100%', alignItems: 'center', justifyContent: "center" }}>
                        <Text>No Room</Text>
                    </View>
                    :
                    <SafeAreaView style={{ flex: 1, width: '100%', alignItems: "center" }} >
                        <FlatList
                            data={
                                search == "" ?
                                    DATA
                                    :
                                    onSearch(DATA)
                            }
                            // contentContainerStyle={styles.listItem}
                            horizontal={false}

                            style={{ width: '100%', maxWidth: 500 }}
                            renderItem={({ item, index }) => (
                                item.password == "" ?
                                    <View key={index} style={{ backgroundColor: '#FFFFFF', maxWidth: 500, width: 'auto', height: 69, marginLeft: 30, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                                        <View style={{ marginRight: 15, marginLeft: 16 }}>
                                            {/* <Image
                                            style={{ height: 40, width: 40 }}
                                            source={{
                                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                                            }}
                                        /> */}
                                            <TextAvatar
                                                backgroundColor={'#707070'}
                                                textColor={'#FFFFFF'}
                                                size={40}
                                                type={'circle'} // optional 
                                            >
                                                {item.roomName}
                                            </TextAvatar>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ fontSize: 16, color: '#000000' }}>{item.roomName}</Text>
                                        </View>
                                        <View style={{ marginRight: 16 }}>
                                            <TouchableOpacity style={{ backgroundColor: "#707070", padding: 8, borderRadius: 2 }} onPress={() => ClickJoin({ item, index })}>
                                                <Text style={{ color: "#FFFFFF" }}>JOIN</Text>
                                            </TouchableOpacity>
                                            {/* <Button title="Join" color="#707070" onPress={() => ClickJoin({ item, index })} /> */}
                                        </View>
                                    </View>
                                    :

                                    <View key={index} style={{ backgroundColor: '#FFFFFF', maxWidth: 500, width: 'auto', height: 69, marginLeft: 30, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                                        <View style={{ marginRight: 15, marginLeft: 16 }}>

                                            <TextAvatar
                                                backgroundColor={'#707070'}
                                                textColor={'#FFFFFF'}
                                                size={40}
                                                type={'circle'} // optional 
                                            >
                                                {item.roomName}
                                            </TextAvatar>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: "row" }}>
                                            <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>{item.roomName}</Text>
                                            <FontAwesome name="lock" style={{ marginEnd: 30 }} size={24} color="#F6CB09" />
                                        </View>


                                        <View style={{ marginRight: 16 }}>
                                            <TouchableOpacity style={{ backgroundColor: "#707070", padding: 8, borderRadius: 2 }} onPress={() => ShowModal({ item, index })} >
                                                <Text style={{ color: "#FFFFFF" }}>JOIN</Text>
                                            </TouchableOpacity>

                                        </View>
                                    </View>

                            )}
                        />
                    </SafeAreaView>
            }




            {/* <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 16, color: '#000000' }}>{DATA[0]}</Text>
                    </View>
                    <View style={{ marginRight: 16 }}>
                        <Button title="Join" color="#707070" onPress={() => navigation.navigate("Chat",{screen: 'Chat', params: {userRedux.roomName}})} />
                    </View>
                </View> */}


            {/* <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>Movie To Free</Text>

                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" onPress={() => navigation.navigate("Chat")} />
                    </View>
                </View>


                <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>CMU</Text>
                        <FontAwesome name="lock" style={{ marginEnd: 30 }} size={24} color="#F6CB09" />
                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" onPress={() => setModalVisible(true)} />
                    </View>
                </View>


                <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>Thai Politics</Text>
                        <FontAwesome name="lock" style={{ marginEnd: 30 }} size={24} color="#F6CB09" />
                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" onPress={() => setModalVisible(true)} />
                    </View>
                </View>

                <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>Cartoons</Text>
                        <FontAwesome name="lock" style={{ marginEnd: 30 }} size={24} color="#F6CB09" />
                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" onPress={() => setModalVisible(true)} />
                    </View>
                </View>

                <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>Sport</Text>
                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" />
                    </View>
                </View>

                <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>Game</Text>

                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" />
                    </View>
                </View>

                <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>PUBG</Text>
                        <FontAwesome name="lock" style={{ marginEnd: 30 }} size={24} color="#F6CB09" />
                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" onPress={() => setModalVisible(true)} />
                    </View>
                </View>

                <View style={{ backgroundColor: '#FFFFFF', width: 360, height: 69, marginLeft: 26, borderBottomColor: '#DEDEDE', borderBottomWidth: 1, marginRight: 26, flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
                    <View style={{ marginRight: 15, marginLeft: 16 }}>
                        <Image
                            style={{ height: 40, width: 40 }}
                            source={{
                                uri: 'https://www.pngkey.com/png/full/493-4939961_circle-chanfer-grey-circle-no-background.png',
                            }}
                        />
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ fontSize: 16, color: '#000000', flex: 1 }}>React Native</Text>

                    </View>
                    <View style={{ marginRight: 16, width: 58 }}>
                        <Button title="Join" color="#707070" />
                    </View>
                </View> */}



        </View >
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    developmentModeText: {
        marginBottom: 20,
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {
        alignItems: 'center',
        marginHorizontal: 50,
    },
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        lineHeight: 24,
        textAlign: 'center',
    },
    helpContainer: {
        marginTop: 15,
        marginHorizontal: 20,
        alignItems: 'center',
    },
    helpLink: {
        paddingVertical: 15,
    },
    helpLinkText: {
        textAlign: 'center',
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        // alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 1.25,
        shadowRadius: 3.84,
        elevation: 5,

    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 10,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 15,
        textAlign: "center"
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 55,
        backgroundColor: 'rgba(0, 0, 0, 0.52)',
    },
    listItem: {
        flex: 1,
        backgroundColor: "#555555",
        alignItems: 'center',
        justifyContent: "center",
        marginTop: 55
    }
});
