const SET_USER = "SET_USER";
const SET_ROOM = "SET_ROOM";
const SET_LOGOUT = "SET_LOGOUT";
const SET_ROOM_CHAT = "SET_ROOM_CHAT"
const SET_ONE_ROOM = "SET_ONE_ROOM"
const SET_USER_LIST = "SET_USER_LIST"
const SET_DATA_ROOM = "SET_DATA_ROOM"

const initialState = {
    name: '',
    roomName: "",
    passwordRoom: '',
    status: false,
    genkey : "",
}

export const setUser = (takeUser , takeKey) => {
    return {
        type: SET_USER,
        name: takeUser,
        genkey : takeKey,
        status: true,
    }
}

export const setRoom = (takeRoom, takePassword) => {

    return {
        type: SET_ROOM,
        roomName: takeRoom,
        passwordRoom: takePassword,
    }
}

export const setLogout = () => {
    return {
        type: SET_LOGOUT
    }
}



export const UserReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                name: action.name,
                genkey : action.genkey,
                status: action.status
            }
        case SET_ROOM:

            return {
                ...state,
                roomName: action.roomName,
                passwordRoom: action.passwordRoom
            }
        case SET_LOGOUT:
            return {
                ...state,
                status: false
            }
        default:
            return state
    }
}

const stateRoom = [
    {
        User: "",
        NameRoom: "",
        Password: "",
    }
]

export const setRoomChat = (takeuser, takeRoom, takepassword) => {
    return {
        type: SET_ROOM_CHAT,
        User: takeuser,
        NameRoom: takeRoom,
        Password: takepassword,
    }
}

export const RoomReducer = (state = stateRoom, action) => {
    switch (action.type) {
        case SET_ROOM_CHAT:
            return [
                ...state,
                action.User,
                action.NameRoom,
                action.Password
            ]
        default:
            return state
    }

}

const oneRoom = {
    room: "",
    index: null,
}

export const setOneRoom = (takeRoom, takeIndex) => {
    return {
        type: SET_ONE_ROOM,
        room: takeRoom,
        index: takeIndex
    }
}

export const OneRoomReducer = (state = oneRoom, action) => {
    switch (action.type) {
        case SET_ONE_ROOM:
            return {
                ...state,
                room: action.room,
                index: action.index

            }
        default:
            return state
    }

}



export const setListUser = (takeUsers) => {
   
    return {
        type: SET_USER_LIST,
        users: takeUsers,
    }
}

export const ListUserReducer = (state = [], action) => {
  
    switch (action.type) {
        case SET_USER_LIST:
            return {
                ...state,
                users: action.users
            }
        default:
            return state
    }

}

export const setData = (takeRoomList) => {
    
    return {
        type: SET_DATA_ROOM,
        roomList: takeRoomList,

    }
}

export const setDataReducer = (state = {}, action) => {
    switch (action.type) {
        case SET_DATA_ROOM:
            return {
                ...state,
                roomList : action.roomList
            }
        default:
            return state
    }

}





// const stateChat = {
//     name: '',
//     roomName: '',
//     messages: '',
// }

// export const ChatReducer = (state = stateChat, action) => {
//     switch (action.type) {
//         case SET_ROOM_CHAT:
//             return state.filter((data)=> data == action.name);
//         default:
//             return state
//     }
// }

// //user ต้องมีการเก็บห้องเป็นของตัวเองได้